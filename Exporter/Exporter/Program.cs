﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NAudio.Midi;
using Newtonsoft.Json;

namespace Exporter
{
    class Program
    {
        const int lanes = 3;

        [Serializable]
        public enum ContentType
        {
            None = 0,
            Required,
            Forbidden
        }

        struct RawTimedEvent
        {
            public long Time;
            public ContentType[] Contents;
        }

        [Serializable]
        struct FormattedTimedEvent
        {
            public float Time;
            public ContentType[] Contents;
        }


        [Serializable]
        struct Track
        {
            public FormattedTimedEvent[] Events;
        }

        static List<RawTimedEvent> events = new List<RawTimedEvent>();
        static double deltaTicksPerQuarterNote = 0.0f;
        static double microsecondsPerQuarterNote = 0.0f;

        static float ConvertTime(long value)
        {
            var result = value / deltaTicksPerQuarterNote * microsecondsPerQuarterNote / 1000000;
            return (float)result;
        }

        static RawTimedEvent ConstructEvent(long time)
        {
            RawTimedEvent result;
            result.Time = time;
            result.Contents = new ContentType[lanes];
            return result;
        }

        static void Register(long time, int lane, ContentType contentType)
        {
            if (contentType == ContentType.None)
                return;
            if (lane >= lanes)
                return;
            for (int i = 0; i < events.Count; ++i)
            {
                if (events[i].Time != time)
                    continue;
                events[i].Contents[lane] = contentType;
                return;
            }
            var e = ConstructEvent(time);
            e.Contents[lane] = contentType;
            events.Add(e);
        }

        static void Main(string[] args)
        {
            string inFile = @"C:\SpiceBerryJam\ggj2016\Exporter\Release\Track.mid";
            string outFile = @"C:\SpiceBerryJam\ggj2016\Assets\Resources\Track.json";
            if (args.Length > 1)
            {
                inFile = args[0];
                outFile = args[1];
            }
            MidiFile midi = new MidiFile(inFile);
            deltaTicksPerQuarterNote = midi.DeltaTicksPerQuarterNote;
            foreach (var eventGroup in midi.Events)
                foreach (var @event in eventGroup)
                {
                    var tempoEvent = @event as TempoEvent;
                    if (tempoEvent != null)
                    {
                        microsecondsPerQuarterNote = tempoEvent.MicrosecondsPerQuarterNote;
                        continue;
                    }
                    var noteEvent = @event as NoteEvent;
                    if (noteEvent != null)
                    {
                        if (noteEvent.CommandCode != MidiCommandCode.NoteOn)
                            continue;
                        if (noteEvent.Channel == 1)
                            continue;
                        var lane = noteEvent.Channel - 2;
                        ContentType contentType = ContentType.None;
                        switch (noteEvent.NoteName)
                        {
                            case "C5":
                                contentType = ContentType.Required;
                                break;
                            case "C6":
                                contentType = ContentType.Forbidden;
                                break;
                        }
                        Register(noteEvent.AbsoluteTime, lane, contentType);
                    }
                }
            var track = new Track();
            track.Events = new FormattedTimedEvent[events.Count];
            for (int i = 0; i < events.Count; ++i)
            {
                track.Events[i].Time = ConvertTime(events[i].Time);
                track.Events[i].Contents = events[i].Contents;
            }
            using (var writer = new StreamWriter(outFile))
                writer.Write(JsonConvert.SerializeObject(track, Formatting.Indented));
        }
    }
}
