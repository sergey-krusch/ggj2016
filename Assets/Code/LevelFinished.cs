﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelFinished: MonoBehaviour
{
    public Text ScoreLabel;

    public void Awake()
    {
        ScoreLabel.text = string.Format("{0}/{1}", Session.Score, Session.TotalRequired);
    }

    public void PlayClick()
    {
        SceneManager.LoadScene("Gameplay", LoadSceneMode.Single);
    }
}