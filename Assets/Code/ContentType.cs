﻿using System;

[Serializable]
public enum ContentType
{
    None = 0,
    Required,
    Forbidden
}