﻿using System;

[Serializable]
public class TimedEvent
{
    public float Time;
    public ContentType[] Contents = new ContentType[Consts.Lanes];
    [NonSerialized]
    public bool[] Picked = new bool[Consts.Lanes];
    [NonSerialized]
    public bool ToBeSwallowed;
}