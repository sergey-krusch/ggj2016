﻿public static class AudioPlayer
{
    public static void Miss()
    {
        AudioOut.Instance.PlaySound("Audio/Sound/Miss", 0.75f, 0.0f);
    }

    public static void Fail()
    {
        AudioOut.Instance.PlaySound("Audio/Sound/Fail", 0.75f, 0.0f);
    }

    public static void Hit(int lane)
    {
        var url = string.Format("Audio/Sound/Hit{0:D2}", lane + 1);
        AudioOut.Instance.PlaySound(url, 1.0f, 0.0f);
    }
}