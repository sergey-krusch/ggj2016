﻿using System.Collections.Generic;
using UnityEngine;

public class Face : MonoBehaviour, IStateReporterListener
{
    public GameObject Tongue;
    public GameObject[] TongueSlots;
    public float[] Positions;

    private int position;
    public int Position
    {
        get
        {
            return position;
        }
        set
        {
            position = value;
            ApplyPosition();
        }
    }

    private Animator animator;
    private RectTransform tongueTransform;
    private EventRepresentation toSwallow;
    private List<GameObject> toDestroy = new List<GameObject>();

    public void Awake()
    {
        animator = GetComponent<Animator>();
        tongueTransform = Tongue.GetComponent<RectTransform>();
    }

    public void Start()
    {
        ApplyPosition();
    }

    public void Swallow(EventRepresentation eventRepresentation)
    {
        if (eventRepresentation == null || eventRepresentation.Event == null)
            return;
        var r = eventRepresentation;
        var e = r.Event;
        e.ToBeSwallowed = true;
        for (int i = 0; i < Consts.Lanes; ++i)
            if (e.Contents[i] == ContentType.Required)
                ++Session.Score;
        toSwallow = r;
        animator.SetTrigger("Swallow");
    }

    private void ApplyPosition()
    {
        var p = tongueTransform.anchoredPosition;
        p.y = Positions[position];
        tongueTransform.anchoredPosition = p;
    }

    void IStateReporterListener.Enter(int nameHash)
    {
    }

    void IStateReporterListener.Exit(int nameHash)
    {
        if (nameHash == Animator.StringToHash("TongueOut"))
        {
            if (toSwallow == null)
                return;
            for (int i = 0; i < Consts.Lanes; ++i)
            {
                if (toSwallow.Event.Contents[i] != ContentType.Required)
                    continue;
                var o = toSwallow.Content[i];
                var ot = o.GetComponent<RectTransform>();
                ot.SetParent(TongueSlots[i].transform, false);
                ot.anchorMin = 0.5f * Vector2.one;
                ot.anchorMax = 0.5f * Vector2.one;
                ot.anchoredPosition = Vector2.zero;
                toDestroy.Add(o);
                toSwallow.Content[i] = null;
            }
        }
        if (nameHash == Animator.StringToHash("TongueIn"))
        {
            foreach (var o in toDestroy)
                Destroy(o);
            toDestroy.Clear();
        }
    }
}
