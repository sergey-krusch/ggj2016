﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TrackPreview: MonoBehaviour
{
    public Track Track;
    public GameObject[] RequiredPrefabs;
    public GameObject ForbiddenPrefab;
    public GameObject ActiveArea;
    public GameObject[] Lanes;
    public Window Window;
    public event Action<int> LanePointerDown; 

    private Dictionary<TimedEvent, EventRepresentation> eventRepresentations;

    public void Awake()
    {
        eventRepresentations = new Dictionary<TimedEvent, EventRepresentation>();
        for (int i = 0; i < Lanes.Length; ++i)
        {
            var idx = i;
            var lane = Lanes[i];
            if (lane == null)
                continue;
            var laneTouchable = lane.GetComponent<Touchable>();
            laneTouchable.PointerDownEvent += () => ActionInvoker.Invoke(LanePointerDown, idx);
        }
    }

    public void Update()
    {
        var currentEvents = Track.Peek(Window);
        foreach (var e in currentEvents)
        {
            var r = GetEventRepresentation(e);
            for (int l = 0; l < Consts.Lanes; ++l)
            {
                if (r.Content[l] == null)
                    continue;
                PlaceContent(r.Content[l], e.Time);
            }
        }
        foreach (var r in eventRepresentations)
        {
            bool active = false;
            foreach (var e in currentEvents)
                if (r.Key == e)
                {
                    active = true;
                    break;
                }
            foreach (var c in r.Value.Content)
                if (c != null && c.activeSelf != active)
                    c.SetActive(active);
        }
        PlaceActiveArea();
    }

    public EventRepresentation GetEventRepresentation(TimedEvent @event)
    {
        if (!eventRepresentations.ContainsKey(@event))
        {
            EventRepresentation representation = new EventRepresentation();
            representation.Event = @event;
            for (int i = 0; i < Consts.Lanes; ++i)
            {
                GameObject prefab = null;
                switch (@event.Contents[i])
                {
                    case ContentType.Forbidden:
                        prefab = ForbiddenPrefab;
                        break;
                    case ContentType.Required:
                        int idx = Random.Range(0, RequiredPrefabs.Length);
                        prefab = RequiredPrefabs[idx];
                        break;
                }
                if (prefab == null)
                    continue;
                var instance = Instantiate(prefab);
                instance.transform.SetParent(Lanes[i].transform, false);
                representation.Content[i] = instance;
            }
            eventRepresentations[@event] = representation;
        }
        return eventRepresentations[@event];
    }

    private void PlaceActiveArea()
    {
        var t = ActiveArea.GetComponent<RectTransform>();
        Vector2 v;
        v = t.anchorMin;
        v.x = (Window.Post - Track.AbsoluteAllowance) / (Window.Pre + Window.Post);
        t.anchorMin = v;
        v = t.anchorMax;
        v.x = (Window.Post + Track.AbsoluteAllowance) / (Window.Pre + Window.Post);
        t.anchorMax = v;
        v = t.anchoredPosition;
        v.x = 0.0f;
        t.anchoredPosition = v;
        v = t.sizeDelta;
        v.x = 0.0f;
        t.sizeDelta = v;
    }

    private void PlaceContent(GameObject content, float time)
    {
        if (content == null)
            return;
        var t = content.GetComponent<RectTransform>();
        var relTime = time - Track.ElapsedTime;
        var shiftedRelTime = relTime + Window.Post;
        var normalizedShiftedTime = shiftedRelTime / (Window.Pre + Window.Post);
        Vector2 a;
        a = t.anchorMin;
        a.x = normalizedShiftedTime;
        t.anchorMin = a;
        a = t.anchorMax;
        a.x = normalizedShiftedTime;
        t.anchorMax = a;
    }
}