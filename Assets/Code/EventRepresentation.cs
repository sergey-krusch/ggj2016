﻿using UnityEngine;

public class EventRepresentation
{
    public TimedEvent Event;
    public GameObject[] Content = new GameObject[Consts.Lanes];
}