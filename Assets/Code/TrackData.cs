﻿using System;

[Serializable]
public struct TrackData
{
    public TimedEvent[] Events;
}