﻿using UnityEngine;
using UnityEngine.UI;

public class Raycastable: Graphic
{
    public override bool Raycast(Vector2 sp, Camera eventCamera)
    {
        return IsActive();
    }

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();
    }
}