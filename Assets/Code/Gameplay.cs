﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Gameplay : MonoBehaviour
{
    public Track Track;
    public Face Face;
    public TrackPreview TrackPreview;
    public Text ScoreLabel;

    public void Awake()
    {
        TrackPreview.LanePointerDown += LanePointerDown;
        Track.Finished += TrackFinished;
        Track.Step += TrackStep;
        Track.HitRequired += TrackHitRequired;
        Track.HitForbidden += TrackHitForbidden;
        Track.HitNothing += TrackHitNothing;
        Track.MissedRequired += TrackMissedRequired;
    }

    public void Start()
    {
        Session.Score = 0;
        Session.TotalRequired = Track.TotalRequired;
    }

    public void Update()
    {
        ScoreLabel.text = string.Format("{0}/{1}", Session.Score, Track.TotalRequired);
    }

    public void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            Track.Input(2);
        if (Input.GetKeyDown(KeyCode.A))
            Track.Input(1);
        if (Input.GetKeyDown(KeyCode.Z))
            Track.Input(0);
    }

    public void ReplayClick()
    {
        SceneManager.LoadScene("Gameplay", LoadSceneMode.Single);
    }

    private void LanePointerDown(int lane)
    {
        Track.Input(lane);
    }

    private void TrackFinished()
    {
        Finish();
    }

    private void TrackStep(TimedEvent e)
    {
        if (e.ToBeSwallowed)
            return;
        var r = TrackPreview.GetEventRepresentation(e);
        if (r == null)
            return;
        for (int i = 0; i < Consts.Lanes; ++i)
            if (e.Contents[i] == ContentType.Required)
            {
                var rr = r.Content[i].GetComponent<RequiredRepresentation>();
                rr.Miss();
            }
    }

    private void TrackHitRequired(TimedEvent e, int lane)
    {
        AudioPlayer.Hit(lane);
        for (int i = 0; i < Consts.Lanes; ++i)
            if (e.Contents[i] == ContentType.Required && !e.Picked[i])
                return;
        Face.Swallow(TrackPreview.GetEventRepresentation(e));
    }

    private void TrackHitForbidden()
    {
        Finish();
    }

    private void TrackHitNothing()
    {
        AudioPlayer.Miss();
    }

    private void TrackMissedRequired()
    {
        AudioPlayer.Fail();
    }

    private void Finish()
    {
        SceneManager.LoadScene("LevelFinished", LoadSceneMode.Single);
    }
}