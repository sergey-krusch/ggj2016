﻿using System;
using UnityEngine.EventSystems;

public class Touchable: UIBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public event Action PointerDownEvent;
    public event Action PointerUpEvent;

    public void OnPointerDown(PointerEventData eventData)
    {
        ActionInvoker.Invoke(PointerDownEvent);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        ActionInvoker.Invoke(PointerUpEvent);
    }
}