﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Track : MonoBehaviour
{
    public string TrackUrl;
    public float TrackBPM;
    public float NormalizedAllowance;
    public event Action Finished;
    public event Action HitNothing;
    public event Action<TimedEvent, int> HitRequired;
    public event Action HitForbidden;
    public event Action MissedRequired;
    public event Action<TimedEvent> Step;

    private float beatInterval;
    private float absoluteAllowance;
    private float elapsedTime;
    private float totalTime;
    private List<TimedEvent> events;
    private int totalRequired;
    private int currentEventIdx;

    public float AbsoluteAllowance
    {
        get
        {
            return absoluteAllowance;
        }
    }

    public float ElapsedTime
    {
        get
        {
            return elapsedTime;
        }
    }

    public int TotalRequired
    {
        get
        {
            return totalRequired;
        }
    }

    public void Awake()
    {
        events = new List<TimedEvent>();
        beatInterval = 60.0f / TrackBPM;
        absoluteAllowance = beatInterval * NormalizedAllowance;
        var dataResource = Resources.Load<TextAsset>("Track");
        var data = JsonUtility.FromJson<TrackData>(dataResource.text);
        events.AddRange(data.Events);
        totalRequired = 0;
        foreach (var e in events)
            for (int i = 0; i < Consts.Lanes; ++i)
                if (e.Contents[i] == ContentType.Required)
                    ++totalRequired;
        currentEventIdx = 0;
    }

    public void Start()
    {
        AudioOut.Instance.PlayMusic(TrackUrl, 1.0f, 0.0f, false);
        totalTime = AudioOut.Instance.CurrentMusic.AudioSource.clip.length;
    }

    public void FixedUpdate()
    {
        elapsedTime = AudioOut.Instance.CurrentMusic.AudioSource.time;
        if (!AudioOut.Instance.CurrentMusic.AudioSource.isPlaying)
        {
            ActionInvoker.Invoke(Finished);
            return;
        }
        if (currentEventIdx < events.Count)
        {
            var e = events[currentEventIdx];
            if (e.Time + absoluteAllowance < elapsedTime)
            {
                for (int i = 0; i < Consts.Lanes; ++i)
                    if (e.Contents[i] == ContentType.Required && !e.Picked[i])
                        ActionInvoker.Invoke(MissedRequired);
                ActionInvoker.Invoke(Step, e);
                ++currentEventIdx;
            }
        }
    }

    public TimedEvent[] Peek(Window window)
    {
        float start = elapsedTime - window.Post;
        float end = elapsedTime + window.Pre;
        List<TimedEvent> result = new List<TimedEvent>();
        foreach (var e in events)
            if (e.Time >= start && e.Time <= end)
                result.Add(e);
        return result.ToArray();
    }

    public bool Input(int lane)
    {
        if (currentEventIdx >= events.Count)
        {
            ActionInvoker.Invoke(HitNothing);
            return false;
        }
        var e = events[currentEventIdx];
        var deltaTime = Mathf.Abs(e.Time - elapsedTime);
        if (deltaTime > absoluteAllowance)
        {
            ActionInvoker.Invoke(HitNothing);
            return false;
        }
        if (e.Picked[lane])
            return false;
        e.Picked[lane] = true;
        if (e.Contents[lane] == ContentType.None)
        {
            ActionInvoker.Invoke(HitNothing);
            return false;
        }
        if (e.Contents[lane] == ContentType.Forbidden)
        {
            ActionInvoker.Invoke(HitForbidden);
            return false;
        }
        if (e.Contents[lane] == ContentType.Required)
            ActionInvoker.Invoke(HitRequired, e, lane);
        return true;
    }
}