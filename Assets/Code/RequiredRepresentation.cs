﻿using UnityEngine;

public class RequiredRepresentation: MonoBehaviour
{
    private Animator animator;

    public void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Hit()
    {
        animator.SetBool("Active", true);
    }

    public void Miss()
    {
        animator.SetBool("Failed", true);
    }
}