﻿using System;

public static class ActionInvoker
{
    public static void Invoke(Action action)
    {
        if (action != null)
            action();
    }

    public static void Invoke<T>(Action<T> action, T arg1)
    {
        if (action != null)
            action(arg1);
    }

    public static void Invoke<T1, T2>(Action<T1, T2> action, T1 arg1, T2 arg2)
    {
        if (action != null)
            action(arg1, arg2);
    }

}