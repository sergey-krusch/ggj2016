Tohunga
=======
A Global Game Jam 2016 Game
-----------
This is a game made by [John Rodygin](https://www.facebook.com/erodygin), [John Bogatskiy](https://www.facebook.com/john.bogatsky), [Cody Elworthy](https://www.facebook.com/CodyElworthy) and [Sergey Krusch](https://www.facebook.com/sergey.krusch) for GGJ2016 at University of Otago, Dunedin, New Zealand.
The binaries for Android (armv7) and Windows can be found [here](http://globalgamejam.org/2016/games/tohunga).